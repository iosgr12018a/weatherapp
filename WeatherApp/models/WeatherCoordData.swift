//
//  WeatherCoordData.swift
//  WeatherApp
//
//  Created by LuisT on 30/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

struct WeatherCoordData:Decodable {
    let lon:Double;
    let lat:Double;
    
}
