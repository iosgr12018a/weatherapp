//
//  WeatherMainData.swift
//  WeatherApp
//
//  Created by LuisT on 30/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

struct WeatherMainData:Decodable {
    let temp:Double;
    let pressure:Int;
    let humidity:Int;
    let tempMin:Int;
    let tempMax:Int;
    
    /*
    En caso de que un atributo del JSON tenga underscore, hay que agregar este enum CodingKeys, de esta manera se puede mapear los nombres de atributos de Swift (en camelCase) contra los atributos de mi JSON.
     IMPORTANTE: si se agrega el CodingKeys, se debe incluir todos los atributos, porque siempre va a leer desde este enum los atributos, ya no desde el struct.
     */
    enum CodingKeys: String, CodingKey {
        case temp
        case pressure
        case humidity
        case tempMin = "temp_min"
        case tempMax = "temp_max"
    }
}
