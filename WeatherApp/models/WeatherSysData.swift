//
//  WeatherSysData.swift
//  WeatherApp
//
//  Created by LuisT on 30/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

struct WeatherSysData:Decodable {
    let type:Int;
    let id:Int;
    let message:Double;
    let country:String;
    let sunrise:Int;
    let sunset:Int;
    
//    init(type:Int, id:Int, message:String, country:String, sunrise:Int, sunset:Int) {
//        self.type = type;
//        self.id = id;
//        self.message = message;
//        self.country = country;
//        self.sunrise = sunrise;
//        self.sunset = sunset;
//    }
}
