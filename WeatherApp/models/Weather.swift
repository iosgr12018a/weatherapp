//
//  Weather.swift
//  WeatherApp
//
//  Created by LuisT on 30/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

struct Weather:Decodable {
    
    let cod:Int;
    let name:String;
    let id:Int;
    let sys:WeatherSysData;
    let dt:Int;
    let clouds: WeatherCloudData;
    let wind:WeatherWindData;
    let visibility:Int;
    let main:WeatherMainData;
    let base: String;
    let weather:[WeatherData];
    let coord: WeatherCoordData;
    
    
}
