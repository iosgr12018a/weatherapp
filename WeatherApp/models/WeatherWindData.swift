//
//  WeatherWindData.swift
//  WeatherApp
//
//  Created by LuisT on 30/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

struct WeatherWindData:Decodable {
    let speed:Double;
    let degree:Double;
    
    enum CodingKeys: String, CodingKey {
        case speed
        case degree = "deg"
    }
}
