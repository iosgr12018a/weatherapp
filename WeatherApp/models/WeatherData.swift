//
//  WeatherData.swift
//  WeatherApp
//
//  Created by LuisT on 30/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

struct WeatherData:Decodable {
    let id:Int;
    let main: String;
    let description:String;
    let icon:String;
    
}
