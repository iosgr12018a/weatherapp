//
//  WeatherViewController.swift
//  WeatherApp
//
//  Created by Kevin Fernando Canacuán Pasquel on 30/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    @IBOutlet weak var resultLabel: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text ?? "quito")&appid=3e78562919281a82ee537cdec2fb83c9"
        
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else {
                print("Error NO data")
                return
            }
        
            guard let weatherInfo = try? JSONDecoder().decode(KevWeatherInfo.self, from: data) else {
                print("Error decoding Weather")
                return
                
            }
            
            
            //print(weatherInfo.weather[0].description) -- Imprimir en consola
            
            //Envíar de hilos secundarios al hilo principal
            DispatchQueue.main.async {
                self.resultLabel.text = "\(weatherInfo.weather[0].description)"
            }
            
        }
        task.resume()
        
    }

}
