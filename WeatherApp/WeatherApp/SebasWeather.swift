//
//  SebasWeather.swift
//  WeatherApp
//
//  Created by Sebastian Guerrero F on 5/30/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

/*
 "main_weather": "rain"
 
 enum CodingKeys: String, CodingKey {
   case weather
   case mainWeather = "main_weather"
 }
 */

struct SebasWeatherInfo: Decodable {
  let weather: [SebasWeather]
}

struct SebasWeather: Decodable {
  let id:Int
  let description:String
}














