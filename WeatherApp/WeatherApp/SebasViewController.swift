//
//  SebasViewController.swift
//  WeatherApp
//
//  Created by Sebastian Guerrero F on 5/29/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit

class SebasViewController: UIViewController {
  
  @IBOutlet weak var cityTextField: UITextField!
  @IBOutlet weak var resultLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  @IBAction func searchButtonPressed(_ sender: Any) {
    let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(cityTextField.text ?? "quito")&appid=f4b14259b70ff9328f483a7729e0c980"
    
    let url = URL(string: urlString)
    let session = URLSession.shared
    let task = session.dataTask(with: url!) { (data, response, error) in
      
      guard let data = data else {
        print("Error NO data")
        return
      }
      
      guard let weatherInfo = try? JSONDecoder().decode(SebasWeatherInfo.self, from: data) else {
        print("Error decoding Weather")
        return
      }
      
      DispatchQueue.main.async {
        self.resultLabel.text = "\(weatherInfo.weather[0].description)"
      }
    }
    task.resume()
  }
}





