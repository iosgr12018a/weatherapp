//
//  KevWeather.swift
//  WeatherApp
//
//  Created by Kevin Fernando Canacuán Pasquel on 30/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import Foundation

/* "main_weather": "rain"
 
 let mainWeather:String
 enum CodingKeys: String, CodingKeys{
 case weather
 case mainWeather = "main_weather"
 }
 */

struct KevWeatherInfo: Decodable {
    
    let weather: [KevWeather]
    
}

struct KevWeather: Decodable {
    
    let id:Int
    let description:String
    
}


