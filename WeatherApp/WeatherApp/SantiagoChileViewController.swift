//
//  SantiagoChileViewController.swift
//  WeatherApp
//
//  Created by LuisT on 29/5/18.
//  Copyright © 2018 Sebastian Guerrero. All rights reserved.
//

import UIKit

/*
 CONSUMO DE REST DESDE iOS.
 Se requiere de 3 cosas:
 - CREACION DE LA URL.
 - CREACION DE LA SESSION
 - CREACION DE LA TAREA
 */
class SantiagoChileViewController: UIViewController {

    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func climaPressed(_ sender: Any) {
        let endpointString:String = "http://api.openweathermap.org/data/2.5/weather?id=3873544&APPID=38607ee9cf9add4456d514c143f46939&units=metric&lang=es"
        
        let URLOpenWeatherSantiagoChile:URL = URL(string: endpointString)!;
        
        let session:URLSession = URLSession.shared; // URLSession es un Singleton compartido que se encuentra en el telefono.
        
        /*
         data: datos enviados por el server
         response: HTTP Headers del response del server
         error: errores que surgieron al realizar el request
         
         AGREGAR: EN EL INFO.PLIST: App Transport Security Settings -> NSAllowsArbitraryLoads = TRUE ==> Dejame hacer request a cualquier parte, es UNA MALA PRACTICA.
         */
        let task:URLSessionTask = session.dataTask(with: URLOpenWeatherSantiagoChile) { (data, response, error)
            in
            
            // con el guard le decimos que si no hay data, que se termine ahi la funcion.
            guard let data = data else {
                print("Error NO data")
                return;
            }
            
            // el guard verifica si puedo decodificar la data.
            // JSONDecoder().decode recibe como parametros el tipo de data a la que quiero decodificar y la data como tal
            guard let deserializedWeather = try? JSONDecoder().decode(Weather.self, from: data) else {
                print("Error decoding Weather")
                return
            }
            
            print(deserializedWeather)
            print("=======================")
            print(deserializedWeather.name)
            print(deserializedWeather.coord.lat)
            print(deserializedWeather.clouds.all)
            print(deserializedWeather.sys.country)
            print(deserializedWeather.wind.speed)
            print(deserializedWeather.main.tempMax)
            print(deserializedWeather.weather[0].description)
            
            // Despache la respuesta asincrona al main thread?
            DispatchQueue.main.async {
                self.nameLabel.text = "\(deserializedWeather.main.temp)ºC - \(deserializedWeather.weather[0].description)";
            }
            
            
        };
        
        task.resume()//LLAMANDO A LA EJECUCION DE LA TAREA
    }
    

    

}
